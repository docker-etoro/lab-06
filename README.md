# Docker Workshop
Lab 06: Using Volumes

---

## Overview 

In this lab we will use named and hosted volumes to allow share data between containers and provide data persistency

## Preparation

 - Create a new folder for the lab:
```
$ mkdir ~/lab-06
$ cd ~/lab-06
```
 
## Using Named Volumes

 - Display existent volumes:
```
$ docker volume ls
```

 - Create a new volume:
```
$ docker volume create my-volume
```

 - Inspect the new volume to find the mountpoint (volume location):
```
$ docker volume inspect my-volume
```
```
[
    {
        "CreatedAt": "2018-06-13T20:36:15Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/my-volume/_data",
        "Name": "my-volume",
        "Options": {},
        "Scope": "local"
    }
]
```

 - Let's run a container and mount the created volume to the /data directory:
```
$ docker run -it -v my-volume:/data --name my-container selaworkshops/busybox:latest
```

 - Create a new file under /data:
```
$ cd /data
$ touch new-file
$ ls
```

 - Open another terminal instance and run other container with the same volume:
```
$ docker run -it -v my-volume:/data --name my-container-2 selaworkshops/busybox:latest
```

 - Inspect the /data folder (the created file will be there):
```
$ cd data
$ ls
```

 - Override the file content in the first container
```
echo new content > /data/new-file
```

 - Check the file content in the second container
```
cat /data/new-file
```

 - Exit from both containers and delete them:
```
$ exit
$ docker rm -f my-container my-container-2
```

 - Ensure the containers were deleted
```
$ docker ps -a
```

 - Run a new container attaching the created volume:
```
$ docker run -it -v my-volume:/data --name new-container selaworkshops/busybox:latest
```

 - Inspect the /data folder (the created file will be there):
```
$ cd data
$ ls
```

 - Exit from the container and delete it:
```
$ exit
$ docker rm -f new-container
```

 - Remove the created volume:
```
$ docker volume rm my-volume
```

 - Display existent volumes:
```
$ docker volume ls
```

## Using Hosted Volumes

 - Create a file in the host filesystem
```
touch ~/lab-06/config.xml
```

 - Run a new container attaching the host directory as volume:
```
$ docker run -it -v ~/lab-06:/data --name new-container selaworkshops/busybox:latest
```

 - Inspect the /data folder in the container (the created file will be there):
```
$ ls /data
```

 - Create a new file in the /data directory (inside the container):
```
$ touch /data/test
```

 - Exit from the container
```
$ exit
```

 - Inspect the hsot server filesystem
```
$ ls ~/lab-06
```

## Cleanup

 - Delete all the containers
```
$ docker rm -f $(docker ps -a -q)
```

 - Remove all the images
```
$ docker rmi -f $(docker images -q)
```

